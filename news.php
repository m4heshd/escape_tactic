<?php
require('wp/wp-load.php');
$y = $_GET['y'];
$m = $_GET['m'];
$d = $_GET['d'];
?>
<?php
$args = array( 'numberposts' => 10, 'order'=> 'DESC', 'orderby' => 'date', 'date_query' => array(array(
			'year'  => $y,
			'month' => $m + 1,
			'day'   => $d,
		),) 
    );
$postslist = get_posts( $args );
foreach ($postslist as $post) :  setup_postdata($post); ?> 
<?php $p_date = get_the_date(); ?>
<div>
  <br>
  <h2 style="font-size: 22px; font-weight: bold;"><?php the_title(); ?>   </h2>
  <div><?php the_excerpt(); ?></div>
  <div class="row" style="background-color: #EBEBEB; padding: 7px 15px; color: #B5B5B5"><?php printf($p_date) ?></div> 
</div>
<?php endforeach; wp_reset_postdata();?>
<br>