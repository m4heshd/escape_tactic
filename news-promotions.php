<!DOCTYPE html>
<!--Work done By Mahesh Bandara Wijerathna-------->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/wow.js"></script>
    <script>
      new WOW().init();
    </script>

    <script src="js/countdown.js" type="text/javascript"></script>
    <link rel="icon" href="favicon.png" sizes="16x16" type="image/png">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.datepicker.css">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>Escape Tactic - News & Promotions</title>
</head>

<body> 

    <nav class="navbar nav-clr">
      <div class="container-fluid">
          <div class="navbar-header col-md-2" style="padding-right:25px;">
              <a href="main.html">
                  <img class="img-responsive" src="images/escape_logo.png">
              </a>
          </div>
          <ul class="nav navbar-nav col-md-8" id="nav_list">
              <li class="active"><a id="nav" href="escape-rooms.html">ESCAPE ROOMS</a></li>
              <li><a id="nav" href="group-events.html">TEAM BUILDING</a></li>
              <li><a id="nav" href="escape-artist.html">#ESCAPEARTISTS</a></li> 
              <li><a id="nav" href="review-escape-tactic.html">REVIEW US</a></li> 
              <li><a id="nav" href="news-promotions.php">NEWS & PROMOTIONS</a></li> 
              <li><a id="nav" href="directions.html">DIRECTIONS</a></li> 
          </ul>
          <div class="navbar-header col-md-2" id="nav_book">
              <a href="book-escape-room.html">
                  <img class="img-responsive" id="book_img" src="images/book_now_nav.png">
              </a>
          </div>
      </div>
  </nav>


  <div class="row">
    <img class="img-responsive" src="images/banner.jpg">
</div>

<!--BANNER END-->

<div class="row" style="padding-top:120px; ">
  <div class="centerBlock wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms" style="animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
    <h2 class="text-center" style="color:#000000; font-size: 32px; font-weight: bold;">NEWS & PROMOTIONS</h2>
  </div>

  <div class="centerBlock wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms" style="animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
    <img class="img-responsive" src="images/two_lines_white.png">
  </div>
  <br>
</div>


<div class="row" id="faq" >
  <div class="centerBlock wow fadeInUp animup" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="col-md-6" style="border-right: thick solid #9F9F9F">
      <h2 style="color:#000000; font-size: 25px; font-weight: bold;">ESCAPE TACTIC NEWS</h2>

      <div class="well" style="margin-bottom: 0">

        <div class="row vcenter">
          <div style="display: inline;">
            <h3 style="padding: 0; padding-right: 10px; margin: 0; font-size: 20px; white-space: nowrap;">Search by date :</h3>
          </div>
          <div style="display: inline;">
            <div class="input-group">
              <input type="text" class="form-control" name="date" id="date" data-select="datepicker" data-locked="25/12/2014;1/1/2015">
              <span class="input-group-btn"><button type="button" class="btn btn-primary" data-toggle="datepicker"><i class="fa fa-calendar"></i></button></span>
            </div>
          </div>
        </div>
      </div>
      <div id="news">
        <?php
        require('wp/wp-blog-header.php');
        ?>
        <?php
        $args = array( 'numberposts' => 3, 'order'=> 'DESC', 'orderby' => 'date' );
        $postslist = get_posts( $args );
        foreach ($postslist as $post) :  setup_postdata($post); ?> 
        <?php $p_date = get_the_date(); ?>
        <div>
          <br>
          <h2 style="font-size: 22px; font-weight: bold;"><?php the_title(); ?>   </h2>
          <div><?php the_excerpt(); ?></div>
          <div class="row" style="background-color: #EBEBEB; padding: 7px 15px; color: #B5B5B5"><?php printf($p_date) ?></div> 
        </div>
      <?php endforeach; wp_reset_postdata();?>
      <br>
    </div>
  </div>
  <div class="col-md-6">
    <h2 class="text-center" style="color:#000000; font-size: 25px; font-weight: bold;">CURRENT PROMOTIONS</h2>
    <div>
      <h2 style="font-size: 22px; font-weight: bold;">PUZZLE OF THE WEEK</h2>
      <p>Think you have the skills to solve some of our brain teasers? Follow Escape Tactic on Facebook
        to solve our Puzzle of the Week. Post your response on your page and tag us. All correct
        answers will be entered into a weekly drawing for a $25 gift card!
      </p>
      <br>
      <h2 style="font-size: 22px; font-weight: bold;">FREE $25 GIFT CARD!</h2>
      <p>Register a group of 20 or more and received a $25 gift card for use on a future date. You don’t
        have to pay for everyone just be the organizer of the group, and other can pay for themselves
        when they arrive to play. We’ll credit you with your gift card in the story once the full group
        has arrived and paid.
      </p>
      <br>
      <h2 style="font-size: 22px; font-weight: bold;">FREE 5 MINUTES</h2>
      <p>60 minutes not enough? It’s easy to get 5 free minutes added to your game. Just post about us
        on Facebook and tag us or send a tweet about Escape Tactic @
        EscapeTacticCLT. Show us the
        post or tweet when you arrive, and we’ll add the time to your game. Sorry, just one 5 minute
        bonus per group.
      </p>
      <br>
    </div>
  </div>

</div>
</div>


<!--FOOTER START-->

<div class="row" id="footertop">
    <h3 class="text-center" style="color:#FFFFFF; font-family: 'Roboto Condensed', sans-serif; margin-bottom:0;">QUICK LINKS</h3>
    <h3 class="text-center" style="color:#FFFFFF; margin-top:0; padding-bottom:15px;">____</h3>
</div>
<div class="row" id="footertoplinks" style="padding-bottom:25px;">
    <div class="col-md-3">
        <a href="about-escape-tactic.html" style="font-size:15px; color:#818181;">About Us</a> </br>
        <a href="contact.html" style="font-size:15px; color:#818181;">Contact Us</a> </br>
        <a href="careers.html" style="font-size:15px; color:#818181;">Careers</a> </br>
        <a href="faq.html" style="font-size:15px; color:#818181;">FAQs</a> </br>
        <a href="reviews.html" style="font-size:15px; color:#818181;">Reviews</a> </br>
    </div>
    <div class="col-md-3">
        <a href="directions.html" style="font-size:15px; color:#818181;">Directions</a> </br>
        <a href="gift-certificate.html" style="font-size:15px; color:#818181;">Gift Certificate</a> </br>
        <a href="privacy.html" style="font-size:15px; color:#818181;">Privacy</a> </br>
        <a href="register.html" style="font-size:15px; color:#818181;">Register</a> </br>
        <a href="" style="font-size:15px; color:#818181;">Site Map</a> </br>
    </div>
    <div class="col-md-3">
        <a href="escape-rooms.html" style="font-size:15px; color:#818181;">Exit Rooms</a> </br>
        <a href="escape-artist.html" style="font-size:15px; color:#818181;">#EscapeArtists</a> </br>
        <a href="news-promotions.php" style="font-size:15px; color:#818181;">News & Promotions</a> </br>
        <a href="review-escape-tactic.html" style="font-size:15px; color:#818181;">Review Us</a> </br>
        <a href="group-events.html" style="font-size:15px; color:#818181;">Team Building</a> </br>
    </div>
    <div class="col-md-3">
        <a href="flight282.html" style="font-size:15px; color:#818181;">Flight 282</a> </br>
        <a href="king-tuts-curse.html" style="font-size:15px; color:#818181;">King Tut's Curse</a> </br>
        <a href="amazon-survival.html" style="font-size:15px; color:#818181;">Amazon Survival</a> </br>
        <a href="seal-team.html" style="font-size:15px; color:#818181;">SEAL Team</a> </br>
        <a href="" style="font-size:15px; color:#818181;">Charalotte Escape</br>Rooms</a> </br>
    </div>
</div>

<div class="row" id="footertop" style="padding-bottom:40px;">
    <div class="centerBlock">
        <a href="">
          <img class="img-responsive" src="images/social.png">
      </a>
  </div>
  <div class="centerBlock">
          <p class="text-center" style="font-size:15px; color:#818181;">Escape Tactic, 520 Clanton Rd., Charlotte, NC 28217</br><a style="font-size:15px; color:#818181;" href="mailto:info@escapetactic.com">info@escapetactic.com</a> 704.749.0773</p>
  </div>
</div>

<div class="row" id="footerbottom">
    <p class="text-center" style="font-size:12px; color:#808080;">Copyright 2016 ESCAPE TACTIC, LLC</p> 
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
<script src="js/jquery.datepicker.js"></script>   
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-77647782-1', 'auto');
  ga('send', 'pageview');
</script>
</body>

</html>